# API Mashup - ColdFusion Swarm Demo

This will be presented at Adobe ColdFusion Summit 2019. Slides for the presentation will be available [here](https://slides.com/mjclemente/coldfusion-docker-swarm-cfsummit-2019#/). It follows the basic structure laid out in the [Starter Swarm Template - ColdFusion](https://gitlab.com/mjclemente/starter-swarm-coldfusion), but includes some more advanced services and integrations, including Nginx, Redis, Portainer, FusionReactor, Traefik, and Consul.

I'll be adding instructions for setting up and using this, as this documentation is obviously insufficient.

*Note that this project is specifically tailored for Adobe ColdFusion. If you're interested in an API mashup using Lucee CFML with Docker Swarm, check out this project:* [`api-mashup-cfml-swarm`](https://gitlab.com/mjclemente/api-mashup-cfml-swarm)